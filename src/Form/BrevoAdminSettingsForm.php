<?php

namespace Drupal\brevo\Form;

use Brevo\Client\ApiException;
use Drupal\brevo\BrevoFactory;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\TypedConfigManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;
use Drupal\brevo\BrevoHandlerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides Brevo configuration form.
 */
class BrevoAdminSettingsForm extends ConfigFormBase {

  /**
   * Brevo handler.
   *
   * @var \Drupal\brevo\BrevoHandlerInterface
   */
  protected $brevoHandler;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('config.typed'),
      $container->get('brevo.handler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, TypedConfigManagerInterface $typedConfigManager, BrevoHandlerInterface $brevo_handler) {
    parent::__construct($config_factory, $typedConfigManager);
    $this->brevoHandler = $brevo_handler;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      BrevoHandlerInterface::CONFIG_NAME,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'brevo_admin_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $entered_api_key = $form_state->getValue('api_key');
    if (!empty($entered_api_key) && $this->brevoHandler->validateBrevoApiKey($entered_api_key) === FALSE) {
      $form_state->setErrorByName('api_key', $this->t("Couldn't connect to the Brevo API. Please check your API settings."));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $this->brevoHandler->validateBrevoLibrary(TRUE);
    $config = $this->config(BrevoHandlerInterface::CONFIG_NAME);

    $form['account'] = [
      '#title' => $this->t('My account'),
      '#type' => 'fieldset',
    ];

    $account = null;
    if (!empty($apiKey = $config->get('api_key'))) {
      try {
        $account = $this->brevoHandler->getBrevoAccount($apiKey);
      }
      catch (ApiException $e) {
        $this->messenger->addError($this->t('Can not fetch Brevo account : @error', ['@error' => $e->getMessage()]));
      }
    }

    if (!empty($account)) {
      // Show account information.
      $accountMarkup = '<p><strong>' . $this->t('You are currently logged in as :') . "</strong></p><p>" . $account->getFirstName() . ' ' . $account->getLastName() . ' - ' . $account->getEmail();
      if (!empty($companyName = $account->getCompanyName())) {
        $accountMarkup .= "<br/>" . $this->t('Company : @company', ['@company' => $companyName]);
      }

      if (!empty($plans = $account->getPlan())) {
        foreach ($plans as $plan) {
          $accountMarkup .= "<br/>" . $plan->getType() . ' - ' . $plan->getCredits() . ' ' . $this->t('credits');
        }
      }

      $accountMarkup .= '</p>';

      $form['account']['details'] = [
        '#type' => 'markup',
        '#markup' => $accountMarkup,
      ];

      $form['account']['logout'] = [
        '#type' => 'submit',
        '#value' => $this->t('Logout'),
        '#submit' => ['::logoutSubmitForm'],
      ];

    } else {
      // Show API key form.
      $form['account']['description'] = [
        '#markup' => $this->t('Please refer to @link for your settings.', [
          '@link' => Link::fromTextAndUrl($this->t('dashboard'), Url::fromUri('https://app-smtp.brevo.com/parameters', [
            'attributes' => [
              'onclick' => "target='_blank'",
            ],
          ]))->toString(),
        ]),
      ];

      $form['account']['api_key'] = [
        '#title' => $this->t('Brevo API Key'),
        '#type' => 'textfield',
        '#required' => TRUE,
        '#description' => $this->t('Enter your @link.', [
          '@link' => Link::fromTextAndUrl($this->t('API key'), Url::fromUri('https://app.brevo.com/settings/keys/api'))->toString(),
        ]),
        '#default_value' => $config->get('api_key'),
      ];
    }

    // Load not-editable configuration object to check actual api key value
    // including overrides.
    $not_editable_config = $this->configFactory()->get(BrevoHandlerInterface::CONFIG_NAME);

    // Don't show other settings until we don't set API key.
    if (empty($not_editable_config->get('api_key'))) {
      return parent::buildForm($form, $form_state);
    }

    // If "API Key" is overridden in settings.php it won't be visible in form.
    // We have to make the field optional and allow to configure other settings.
    if (empty($config->get('api_key')) && !empty($not_editable_config->get('api_key'))) {
      $form['account']['api_key']['#required'] = FALSE;
    }

    $form['automation'] = [
      '#title' => $this->t('Automation'),
      '#type' => 'fieldset',
    ];

    if (!empty($account) && !empty($accountMarketingAutomation = $account->getMarketingAutomation())) {
      // Get the account automation identifier.
      $form['automation']['activate_marketing_automation'] = [
        '#title' => $this->t('Activate Marketing Automation through Brevo'),
        '#type' => 'checkbox',
        '#description' => $this->t('Check this box if you want to use @link  to track your website activity.', [
            '@link' => Link::fromTextAndUrl($this->t('Brevo Automation'), Url::fromUri('https://automation.brevo.com/parameters'))->toString(),
          ]) . "<br/>" . $this->t('When enabled, the Brevo JavaScript SDK will be added to all pages of your website.'),
        '#default_value' => $config->get('activate_marketing_automation'),
      ];

      $form['automation']['client_key'] = [
        '#title' => $this->t('Marketing Automation client key'),
        '#type' => 'textfield',
        '#disabled' => TRUE,
        '#value' => $accountMarketingAutomation->getKey(),
        '#description' => $this->t('This is your client key which is automatically fetched from Brevo\'s API.'),
      ];

    } else {
      // The admin first have to enable the automations in Brevo.
      $form['automation']['activate_marketing_automation'] = [
        '#type' => 'markup',
        '#markup' => $this->t('Please enable the automation in your @link dashboard before coming back here.', [
            '@link' => Link::fromTextAndUrl($this->t('Brevo Automation'), Url::fromUri('https://automation.brevo.com/parameters'))->toString(),
          ]),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config_keys = ['api_key', 'activate_marketing_automation', 'client_key'];
    $brevo_config = $this->config(BrevoHandlerInterface::CONFIG_NAME);
    foreach ($config_keys as $config_key) {
      if ($form_state->hasValue($config_key)) {
        $brevo_config->set($config_key, $form_state->getValue($config_key));
      }
    }
    $brevo_config->save();

    $this->messenger()->addMessage($this->t('The configuration options have been saved.'));
  }

  /**
   * Custom submit handler for the logout button.
   */
  public function logoutSubmitForm(array &$form, FormStateInterface $form_state) {
    $brevo_config = $this->config(BrevoHandlerInterface::CONFIG_NAME);
    $brevo_config->clear('api_key');
    $brevo_config->clear('client_key');
    $brevo_config->save();

    $this->messenger()->addStatus($this->t('Your Brevo account was disconnected from your Drupal website.'));
    $form_state->setRedirect('<current>');
  }
}
